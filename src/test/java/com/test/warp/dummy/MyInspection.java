/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.warp.dummy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.warp.Inspection;
import org.jboss.arquillian.warp.servlet.BeforeServlet;
import org.jboss.arquillian.warp.spi.WarpCommons;
import org.junit.Assert;

/**
 *
 * @author LeVyAnh
 */
public class MyInspection extends Inspection {

    private static final long serialVersionUID = 1L;
    @ArquillianResource
    HttpServletRequest request;
    @ArquillianResource
    HttpServletResponse response;

    @BeforeServlet
    public void beforeServlet() {

        System.out.println("Hi server, here is my initial request!");

        Assert.assertNotNull("request must be enriched", request.getHeader(WarpCommons.ENRICHMENT_REQUEST));

        Assert.assertNotNull("request context must be available", request);

        Assert.assertNotNull("responses enrichment is set before servlet processing",
                response.getHeader(WarpCommons.ENRICHMENT_RESPONSE));
    }
}
